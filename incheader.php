<?php
function writeHeader($title, $here = 0, $menu = true){
	if ($title !== '')
		$title = "$title - ComputerFestival 2014 投票システム";
	else
		$title = 'ComputerFestival 2014 投票システム';
	
	$array = ['', '', '', '', ''];
	$array[$here] = ' id="here"';
	
	if (is_loggedin() && isset($_SESSION['user_info'])){
		$session_html = "<strong>".$_SESSION['user_info'] -> name."</strong>(<a href=\"".ROOT."signout\">ログアウト</a>)";
	}else{
		$session_html = "<a href=\"".ROOT."signin\">ログイン</a>";
	}
	
?>
<!DOCTYPE html>
<html>
	<head>
	<meta http-equiv="content-type" content="text/html; charset=utf-8" />
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<link rel="stylesheet" type="text/css" href="<?=ROOT?>assets/style.css" />
	<title><?=$title?></title>
	</head>
	<body>
		<header>
			<nav class="center">
			<a href="<?=ROOT?>">
					<img id="logo" src="<?=ROOT?>assets/images/logo.png" alt="ComputerFestival 2014" />
				</a>
				<ul id="menu">
					<li<?=$array[1]?>><a href="<?=ROOT?>">トップ</a></li>
					<li<?=$array[2]?>><a href="<?=ROOT?>software">ソフトウェア部門</a></li>
					<li<?=$array[3]?>><a href="<?=ROOT?>media">メディア<br />コンテンツ部門</a></li>
					<li><a href="http://comfes2014.tct-newmedia.net/">Comfes2014<br />サイトへ</a></li>
					<div class="clear"></div>
				</ul>
			</nav>
		</header>
<?php
	if($menu){
?>
		<div id="session">
			<div class="center">
				<?=$session_html?>
			</div>
		</div>
<?php
	}
}
?>
