<?php
require 'include.php';

if (!is_loggedin()){
	redirect_to_top();
	exit();
}

if (isset($_GET['genre']) && isset($_GET['id'])){
	$genre = $_GET['genre'];
	$id    = $_GET['id'];
}else{
	redirect_to_top();
	exit();
}
if (!(is_string($genre) || is_numeric($id))){
	redirect_to_top();
	exit();
}

$genre_num = 0;
if ($genre === "s"){
	$genre_num = 1;
	$back_url  = 'software';
}
if ($genre === "m"){
	$genre_num = 2;
	$back_url = 'media';
}
if ($genre_num === 0){
	redirect_to_top();
	exit();
}

try{
	$db = new PDO('sqlite:./vote.db');
	$db -> setAttribute(PDO::ATTR_EMULATE_PREPARES, false);
	
	$sql = $db -> prepare('SELECT sakuhin.lid AS id,sakuhin.title AS title,authors.name AS author,schools.name AS school FROM (sakuhin INNER JOIN authors ON sakuhin.author == authors.id) INNER JOIN schools ON authors.school == schools.id WHERE sakuhin.genre == ? AND sakuhin.lid == ?');
	$sql -> bindValue(1, $genre_num, PDO::PARAM_INT);
	$sql -> bindValue(2, $id,        PDO::PARAM_INT);
	$sql -> execute();
	$res = $sql -> fetchAll(PDO::FETCH_ASSOC);
	if (!$res)
		throw new Exception('お探しの作品が見つかりませんでした。');
	$t   = $res[0];
	
	$csrf_token = get_csrf_token();
	$_SESSION['csrf_token'] = $csrf_token;
	
}catch(Exception $e){
	$errormessage = $e -> getMessage();
}

if (!empty($errormessage))
	$error_html = "<p class=\"error\">$errormessage</p>\n";

writeHeader('投票確認', $genre_num+1);
?>
		<div id="container" class="center">
			<h1>投票確認</h1>
			<?=$error_html?>
<?php
if ($res){
?>
			<p>
				以下の作品に投票しますか？<br />
				よろしければ確認リンクをクリックしてください。
			</p>
			<table id="votelist">
				<tr class="head">
					<th class="symbol"></th>
					<th class="number">#</th>
					<th class="title">Title</th>
					<th colspan="2">Author</th>
					<th class="vote"></th>
				</tr>
				<tr>
					<td></td>
					<td><?=h(strtoupper($genre))?>-<?=h($t['id'])?></td>
					<td><?=h($t['title'])?></td>
					<td><?=str_replace('＆', '<br />', h($t['author']))?></td>
					<td><?=h($t['school'])?></td>
					<td></td>
				</tr>
			</table>
			<div class="interface">
				この作品に投票します。<br />
				<strong class="attention">!!! この操作は取り消すことができません !!!</strong>
				<div class="container">
				<a class="btn confirm" href="<?=ROOT?>vote/<?=h($genre)?><?=h($t['id'])?>/confirm/<?=$csrf_token?>">
						確認
					</a>
					<a class="btn cancel" href="<?=ROOT.$back_url?>">
						キャンセル
					</a>
				</div>
			</div>
<?php
}
?>
		</div>
<?php
writeFooter();
?>
