<?php
require 'include.php';

if (!is_loggedin()){
	redirect_to_top();
	exit();
}
if (!is_admin()){
	redirect_to_top();
	exit();
}

try{
	$db = new PDO('sqlite:./vote.db');
	$db -> setAttribute(PDO::ATTR_EMULATE_PREPARES, false);
	$sql = $db -> prepare('SELECT sakuhin.lid AS id,sakuhin.title AS title,authors.name AS author,schools.name AS school, voteresult.cnt AS vote FROM ((sakuhin INNER JOIN authors ON sakuhin.author == authors.id) INNER JOIN schools ON authors.school == schools.id) LEFT OUTER JOIN (SELECT sid, count(*) AS cnt FROM votes GROUP BY sid) AS voteresult ON voteresult.sid == sakuhin.id WHERE sakuhin.genre == 2 ORDER BY vote DESC');
	$sql -> execute();
	$res = $sql -> fetchAll(PDO::FETCH_ASSOC);

}catch(Exception $e){
	$errormessage = $e -> getMessage();
}

if (!empty($errormessage))
	$error_html = "<p class=\"error\">$errormessage</p>\n";

writeHeader('メディアコンテンツ部門結果', 3);
?>
		<div id="container" class="center">
			<h1>メディアコンテンツ部門結果</h1>
			<?=$error_html?>
<?php
if ($res){
?>
			<p>メディアコンテンツ部門の結果を表示しています。</p>
			<table id="votelist">
				<tr class="head">
					<th class="symbol"></th>
					<th class="number">#</th>
					<th class="title">Title</th>
					<th colspan="2">Author</th>
					<th class="vote">Count</th>
				</tr>
<?php
foreach($res as $t){
?>
				<tr>
					<td></td>
					<td>M-<?=h($t['id'])?></td>
					<td><?=h($t['title'])?></td>
					<td><?=str_replace('＆', '<br />', h($t['author']))?></td>
					<td><?=h($t['school'])?></td>
					<td><?=h($t['vote'])?></td>
				</tr>
<?php
}
?>
			</table>
<?php
}
?>
		</div>
<?php
writeFooter();
?>
