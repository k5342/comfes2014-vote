<?php
require 'incheader.php';
require 'incfooter.php';

define('ROOT', dirname($_SERVER['PHP_SELF']).'/');

function h($str){
	return htmlspecialchars($str, ENT_QUOTES);
}

function is_loggedin(){
	return isset($_SESSION['logged_in']) && $_SESSION['logged_in'];
}

function is_admin(){
	return isset($_SESSION['user_info']) && $_SESSION['user_info'] -> admin;
}

function redirect_to_top(){
	header('Location: '.ROOT);
}

function get_csrf_token() {
	return md5(uniqid(mt_rand(), true));
}

class User{
	public $id;
	public $name;
	public $admin;
	
	function __construct($id, $name, $admin){
		$this -> id     = (int)$id;
		$this -> name   = $name;
		$this -> admin  = $admin;
	}
}

session_start();

$res = [];
$success_html = "\n";
$error_html = "\n";
?>
