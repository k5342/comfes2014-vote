<?php
require 'include.php';

$modenum  = (int)$_GET['mode'];
$modename = ['', 'ソフトウェア', 'メディアコンテンツ'];
$modechar = ['', 'S', 'M'];
$modelink = ['', 's', 'm'];
$_SESSION['csrf_token'] = '';

if (!is_loggedin()){
	redirect_to_top();
	exit();
}

try{
	$db = new PDO('sqlite:./vote.db');
	$db -> setAttribute(PDO::ATTR_EMULATE_PREPARES, false);
	
	# votes
	$lim = $db -> prepare('SELECT id FROM votes WHERE genre == ? and uid == ?');
	$lim -> bindValue(1, $modenum, PDO::PARAM_INT);
	$lim -> bindValue(2, $_SESSION['user_info'] -> id, PDO::PARAM_INT);
	$lim -> execute();
	$res = $lim -> fetchAll(PDO::FETCH_ASSOC);
	$votes = count($res);
	
	$sql = $db -> prepare('SELECT sakuhin.lid AS id,sakuhin.title AS title,authors.name AS author,schools.name AS school, myvotes.sid AS vote FROM ((sakuhin INNER JOIN authors ON sakuhin.author == authors.id) INNER JOIN schools ON authors.school == schools.id) LEFT OUTER JOIN (SELECT * FROM votes WHERE votes.uid == ?) AS myvotes ON myvotes.sid == sakuhin.id WHERE sakuhin.genre == ? ORDER BY id');
	$sql -> bindValue(1, $_SESSION['user_info'] -> id, PDO::PARAM_INT);
	$sql -> bindValue(2, $modenum,                     PDO::PARAM_INT);
	$sql -> execute();
	$res = $sql -> fetchAll(PDO::FETCH_ASSOC);
	
}catch(Exception $e){
	$errormessage = $e -> getMessage();
}

if (!empty($errormessage))
	$error_html = "<p class=\"error\">$errormessage</p>\n";

writeHeader("$modename[$modenum]部門", $modenum+1);
?>
		<div id="container" class="center">
			<h1><?=$modename[$modenum]?>部門作品一覧</h1>
			<?=$error_html?>
<?php
if ($res){
?>
			<p>
				作品一覧から投票を行います。<br />
				<strong class="attention">投票は各部門3回のみずつ行うことができます。</strong><br />
				(残り<span class="attention bigger"><?=(3-$votes)?></span>票いれることができます。)
			</p>
			<div id="votelist">
				<div class="head">
					<div class="symbol">✔</div>
					<div class="number">#</div>
					<div class="title">Title</div>
					<div class="author">Author</div>
					<div class="vote"></div>
				</div>
<?php
foreach($res as $t){
	if($t['vote']){
		$symbol    = '✔';
		$status    = ' class="active"';
		$vote_link = '<span class="disabled">投票済</span>';
	}else{
		$symbol    = '';
		$status    = '';
		if ($votes < 3)
			$vote_link = '<a href="'.ROOT.'vote/'.$modelink[$modenum].h($t['id']).'">投票</a>';
		else
			$vote_link = '';
	}
?>
				<div<?=$status?>>
					<div class="symbol"><?=$symbol?></div>
					<div class="number"><?=$modechar[$modenum]?>-<?=h($t['id'])?></div>
					<div class="title"><?=h($t['title'])?></div>
					<div class="name"><?=str_replace('＆', '<br />', h($t['author']))?></div>
					<div class="school"><?=h($t['school'])?></div>
					<div class="vote"><?=$vote_link?></div>
				</div>
<?php
}
?>
			</div>
<?php
}
?>
		</div>
<?php
writeFooter();
?>
