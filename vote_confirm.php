<?php
require 'include.php';
$success = false;

if (!is_loggedin()){
	redirect_to_top();
	exit();
}

if (isset($_GET['genre']) && isset($_GET['id'])){
	$genre =      $_GET['genre'];
	$slid  = (int)$_GET['id'];
}else{
	redirect_to_top();
	exit();
}
if (!(is_string($genre) || is_numeric($slid))){
	redirect_to_top();
	exit();
}

$genre_num = 0;
if ($genre === "s"){
	$genre_num = 1;
	$back_url  = 'software';
}
if ($genre === "m"){
	$genre_num = 2;
	$back_url = 'media';
}
if ($genre_num === 0){
	redirect_to_top();
	exit();
}

try{
	$db = new PDO('sqlite:./vote.db');
	$db -> setAttribute(PDO::ATTR_EMULATE_PREPARES, false);
	
	if (!isset($_SESSION['csrf_token']) || !isset($_GET['csrf_token']) || is_array($_GET['csrf_token']))
		throw new Exception('セッションが無効です。もう一度最初からやり直してください。');
	
	if ($_SESSION['csrf_token'] !== $_GET['csrf_token'])
		throw new Exception('セッションの認証情報が不正です。もう一度最初からやり直してください。');
	
	# is exist?
	$bfr = $db -> prepare('SELECT id FROM sakuhin WHERE genre == ? and lid == ?');
	$bfr -> bindValue(1, $genre_num, PDO::PARAM_INT);
	$bfr -> bindValue(2, $slid,      PDO::PARAM_INT);
	$bfr -> execute();
	$res = $bfr -> fetchAll(PDO::FETCH_ASSOC);
	if(!$res)
		throw new Exception('投票処理中にエラーが発生しました。');
	
	$sid       = (int)$res[0]['id'];
	$uid       = $_SESSION['user_info'] -> id;
	$ip        = $_SERVER['REMOTE_ADDR'];
	$timestamp = time();
	$ua        = $_SERVER['HTTP_USER_AGENT'];
	
	# finish limit?
	$lim = $db -> prepare('SELECT id FROM votes WHERE genre == ? and uid == ?');
	$lim -> bindValue(1, $genre_num, PDO::PARAM_INT);
	$lim -> bindValue(2, $_SESSION['user_info'] -> id, PDO::PARAM_INT);
	$lim -> execute();
	$res = $lim -> fetchAll(PDO::FETCH_ASSOC);
	if(count($res) >= 3)
		throw new Exception('投票の上限を超えています。');
	
	# is duplicate?
	$res = [];
	$dup = $db -> prepare('SELECT id FROM votes WHERE sid == ? and uid == ?');
	$dup -> bindValue(1, $sid, PDO::PARAM_INT);
	$dup -> bindValue(2, $uid, PDO::PARAM_INT);
	$dup -> execute();
	$res = $dup -> fetchAll(PDO::FETCH_ASSOC);
	if($res)
		throw new Exception('既に投票済みの作品です。');

	# vote
	$res = [];
	$sql = $db -> prepare('INSERT INTO votes VALUES(null, :genre, :sid, :slid, :uid, :ip, :timestamp, :ua)');
	$sql -> bindValue(':genre',     $genre_num, PDO::PARAM_INT);
	$sql -> bindValue(':sid',       $sid,       PDO::PARAM_INT);
	$sql -> bindValue(':slid',      $slid,      PDO::PARAM_INT);
	$sql -> bindValue(':uid',       $uid,       PDO::PARAM_INT);
	$sql -> bindValue(':ip',        $ip,        PDO::PARAM_STR);
	$sql -> bindValue(':timestamp', $timestamp, PDO::PARAM_STR);
	$sql -> bindValue(':ua',        $ua,        PDO::PARAM_STR);
	$res = $sql -> execute();
	if (!$res)
		throw new Exception('投票処理に失敗しました。');
	
	$success = true;
	
}catch(Exception $e){
	$errormessage = $e -> getMessage();
}

if (!empty($errormessage))
	$error_html = "<p class=\"error\">$errormessage</p>\n";

writeHeader('投票処理', $genre_num+1);
?>
		<div id="container" class="center">
			<h1>投票処理</h1>
			<div class="interface">
			<?=$error_html?>
<?php
if ($res && $success){
?>
				投票処理が完了しました。<br />
<?php
}
?>
				<div class="container">
					<a class="btn confirm" href="<?=ROOT.$back_url?>">
						戻る
					</a>
				</div>
			</div>
		</div>
<?php
writeFooter();
?>
