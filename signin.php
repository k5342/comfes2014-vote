<?php
require 'include.php';
$errormessage = '';
if (isset($_POST['submit'])){
	try{
		$name = $_POST['name'];
		$password = $_POST['password'];
		$password_hash = sha1("".$name.$password."hogehogehoge");
		
		if (empty($name) || empty($password))
			throw new Exception('Input box can\'t be blank.');
		
		$db = new PDO('sqlite:./vote.db');
		$db -> setAttribute(PDO::ATTR_EMULATE_PREPARES, false);
		
		$sql = $db -> prepare('SELECT id,name,admin FROM users WHERE password_hash == ? and name == ?');
		var_dump($db -> errorInfo());
		$sql -> bindParam(1, $password_hash, PDO::PARAM_STR);
		$sql -> bindParam(2, $name,          PDO::PARAM_STR);
		$sql -> execute();
		$res = $sql -> fetchAll(PDO::FETCH_ASSOC);
		if (!$res)
			throw new Exception('IDとパスワードの組み合わせが間違っています。');
		
		$tmp = $res[0];
		$id     = $tmp['id'];
		$name   = $tmp['name'];
		$admin  = ($tmp['admin'] === 'true') ?  true : false;
		
		if (count($res) === 1){
			$_SESSION['logged_in'] = true;
			$_SESSION['user_info'] = new User($id, $name, $admin);
			session_regenerate_id(true);
			redirect_to_top();
			exit();
		}else{
			throw new Exception('IDとパスワードの組み合わせが間違っています。 ('.count($res).')');
		}
	}catch(Exception $e){
		$errormessage = $e -> getMessage();
	}
}

if (!empty($errormessage))
	$error_html = "<p class=\"error\">$errormessage</p>\n";

writeHeader('ログイン', 0, false);
?>
<div id="container" class="center">
	<h1>ログイン</h1>
		<form method="POST" class="interface" >
			<?=$error_html?>
			<p>
				<span>ID:</span>
				<input type="text" name="name" />
			</p>
			<p>
				<span>Password:</span>
				<input type="password" name="password" />
			</p>
			<input type="submit" name="submit" />
		</form>
	</div>
</div>
